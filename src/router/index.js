import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue"
import Dashboard from "../views/Dashboard.vue";
import Muisca from "../views/Muisca.vue";
// import PanelSent from "../components/company/PanelSent.vue"; 
// import PanelReceived from "../components/company/PanelReceived.vue";
import PanelBilling from "../components/company/PanelBilling.vue";
import CreateProduct from "../components/person/CreateProduct.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "dashboard",
    component: Dashboard,
    children: [
      {
        path: "/",
        name: "facturacionElectronica",
        component: CreateProduct
      },
      // {
      //   path: "/dashboard/enviados",
      //   name: "panelsent",
      //   component: PanelSent
      // },
      // {
      //   path: "/dashboard/recibidos",
      //   name: "panelreceived",
      //   component: PanelReceived
      // },
      {
        path: "/dashboard/facturador",
        name: "panelbilling",
        component: PanelBilling
      }
      
    ]
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/muiscadian",
    name: "muiscadian",
    component: Muisca
  }
  // {
  //   path: "/about",
  //   name: "about",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/About.vue")
  // }
];

const router = new VueRouter({
  routes
});

export default router;
