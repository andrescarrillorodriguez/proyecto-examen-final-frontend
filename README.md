# Frontend Examen Final (Emulación de Facturación Electrónica)

## Configuración del Proyecto
```
npm install
```

### Compilación y hot-reload para el desarrollo
```
npm run serve
```

### Compilar y minimizar para puesta en producción.
```
npm run build
```

### Limpiar y Corregir Archivos
```
npm run lint
```

